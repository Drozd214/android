package com.oleksandr.geoquiz.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.oleksandr.geoquiz.EXTRA_ANSWER_IS_TRUE
import com.oleksandr.geoquiz.EXTRA_ANSWER_SHOWN
import com.oleksandr.geoquiz.TAG
import com.oleksandr.geoquiz.R
import com.oleksandr.geoquiz.REQUEST_CODE_CHEAT
import com.oleksandr.geoquiz.cheat.CheatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(),
    MainContract.View {


    private val mPresenter: MainPresenter = MainPresenter(this)




    //для перевірки життєвого циклу Activity
    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "onCreate")
        mPresenter.getInitData()

        //ініціалізація лісенерів
        initButtons()
        initTextViews()
    }


    private fun initButtons() {
        true_button.setOnClickListener {
            mPresenter.answerButtonClicked(true)

        }


        false_button.setOnClickListener {
            mPresenter.answerButtonClicked(false)
        }


        next_button.setOnClickListener {
            mPresenter.nextButtonClicked()
        }


        previous_button.setOnClickListener {
            mPresenter.previousButtonClicked()
        }


        cheat_button.setOnClickListener {
            mPresenter.cheatButtonClicked()
        }
    }


    private fun initTextViews() {
        question_text_view.setOnClickListener {
            mPresenter.nextButtonClicked()
        }
    }


    //отримання результатів з відповідних Activity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //спочатку перевірка чи Activity закрили через кнопку Back
        //чи через прописану власну дію з встановленням даних
        if(resultCode != Activity.RESULT_OK) { return }

        //перевірка яка саме Activity дає результати (дані)
        if(requestCode == REQUEST_CODE_CHEAT) {
            if(data == null) { return }
            mPresenter.updateCheatStatus(wasAnswerShown(data))
        }
    }


    override fun showToast(textId: Int) {
        Toast.makeText(this, textId, Toast.LENGTH_SHORT).show()
    }


    override fun updateQuestion(questionId: Int) {
        question_text_view.text = getString(questionId)
    }


    override fun launchCheatScreen(answer: Boolean) {
        val intent = Intent(this, CheatActivity::class.java)
        intent.putExtra(EXTRA_ANSWER_IS_TRUE, answer)
        startActivityForResult(intent, REQUEST_CODE_CHEAT)
    }


    private fun wasAnswerShown(result: Intent) : Boolean {
        return result.getBooleanExtra(EXTRA_ANSWER_SHOWN, false)
    }


}
