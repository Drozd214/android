package com.oleksandr.geoquiz.main

import com.oleksandr.geoquiz.models.Question
import com.oleksandr.geoquiz.R

class MainPresenter(
    private val mView: MainContract.View
) : MainContract.ActionListener {


    private var mIsCheater: Boolean = false
    var mCurrentIndex = 0
    private val mQuestionsList = listOf<Question>(
        Question(
            R.string.question_oceans,
            true
        ),
        Question(
            R.string.question_mideast,
            false
        ),
        Question(
            R.string.question_africa,
            false
        ),
        Question(
            R.string.question_americas,
            true
        ),
        Question(
            R.string.question_asia,
            true
        )
    )




    override fun answerButtonClicked(answer: Boolean) {
        if(mIsCheater) {
            mView.showToast(R.string.judgment_toast)
        } else {
            if (mQuestionsList[mCurrentIndex].mAnswerTrue == answer) {
                mView.showToast(R.string.correct_toast)
            } else {
                mView.showToast(R.string.incorrect_toast)
            }
        }
    }


    override fun cheatButtonClicked() {
        mView.launchCheatScreen(mQuestionsList[mCurrentIndex].mAnswerTrue)
    }


    override fun nextButtonClicked() {
        mCurrentIndex = (mCurrentIndex + 1) % mQuestionsList.size
        mView.updateQuestion(mQuestionsList[mCurrentIndex].mTextResId)
        updateCheatStatus(false)
    }


    override fun previousButtonClicked() {
        mCurrentIndex = (mCurrentIndex - 1 + mQuestionsList.size) % mQuestionsList.size
        mView.updateQuestion(mQuestionsList[mCurrentIndex].mTextResId)
        updateCheatStatus(false)
    }


    override fun getInitData() {
        mView.updateQuestion(mQuestionsList[0].mTextResId)
    }


    override fun updateCheatStatus(isCheat: Boolean) {
        mIsCheater = isCheat
    }


}