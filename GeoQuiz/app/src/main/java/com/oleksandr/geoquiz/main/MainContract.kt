package com.oleksandr.geoquiz.main

interface MainContract {

    interface View {

        fun showToast(textId: Int)

        fun updateQuestion(questionId: Int)

        fun launchCheatScreen(answer: Boolean)
    }

    interface ActionListener {

        fun answerButtonClicked(answer: Boolean)
        fun cheatButtonClicked()
        fun nextButtonClicked()
        fun previousButtonClicked()

        fun getInitData()

        fun updateCheatStatus(isCheat: Boolean)
    }
}