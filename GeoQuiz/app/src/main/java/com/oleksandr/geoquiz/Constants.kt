package com.oleksandr.geoquiz

internal val EXTRA_ANSWER_IS_TRUE = "com.bignerdranch.android.geoquiz.answer_is_true"
internal val EXTRA_ANSWER_SHOWN = "com.bignerdranch.android.geoquiz.answer_shown"

internal val TAG: String = "MainActivity"

const val REQUEST_CODE_CHEAT = 0