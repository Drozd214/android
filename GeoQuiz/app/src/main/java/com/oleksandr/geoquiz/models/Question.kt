package com.oleksandr.geoquiz.models

data class Question(val mTextResId: Int,
               val mAnswerTrue: Boolean)
