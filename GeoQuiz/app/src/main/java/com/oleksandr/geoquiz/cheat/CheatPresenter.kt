package com.oleksandr.geoquiz.cheat

import com.oleksandr.geoquiz.R

class CheatPresenter (
    private val mView: CheatContract.View
) : CheatContract.ActionListener {


    override fun showAnswerButtonClicked(answer: Boolean) {
        if(answer) {
            mView.showAnswer(R.string.true_button)
        } else {
            mView.showAnswer(R.string.false_button)
        }

        mView.setAnswerShownResult()
    }


}