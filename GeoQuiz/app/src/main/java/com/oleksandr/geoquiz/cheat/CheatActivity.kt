package com.oleksandr.geoquiz.cheat

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.oleksandr.geoquiz.EXTRA_ANSWER_IS_TRUE
import com.oleksandr.geoquiz.EXTRA_ANSWER_SHOWN
import com.oleksandr.geoquiz.R
import kotlinx.android.synthetic.main.activity_cheat.*
import kotlinx.android.synthetic.main.activity_main.*

class CheatActivity : AppCompatActivity(), CheatContract.View {


    private val mPresenter = CheatPresenter(this)




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheat)

        val answer = intent.getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false)

        version_text_view.text = getString(R.string.api_level, android.os.Build.VERSION.SDK_INT)

        show_answer_button.setOnClickListener {
            mPresenter.showAnswerButtonClicked(answer)
        }
    }


    //встановлення даних для результату для батьківської Activity
    override fun setAnswerShownResult() {
        val intent = Intent()
        intent.putExtra(EXTRA_ANSWER_SHOWN, true)
        setResult(Activity.RESULT_OK, intent)
    }


    override fun showAnswer(answerId: Int) {
        answer_text_view.text = getString(answerId)
    }


}
