package com.oleksandr.geoquiz.cheat

interface CheatContract {

    interface View{
        fun setAnswerShownResult()

        fun showAnswer(answerId: Int)
    }


    interface ActionListener {
        fun showAnswerButtonClicked(answer: Boolean)
    }

}