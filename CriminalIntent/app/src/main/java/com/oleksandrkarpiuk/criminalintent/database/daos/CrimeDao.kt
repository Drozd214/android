package com.oleksandrkarpiuk.criminalintent.database.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.oleksandrkarpiuk.criminalintent.database.models.Crime

@Dao
interface CrimeDao {

    @Query("SELECT * FROM crimes")
    fun getCrimes() : List<Crime>

    @Query("SELECT * FROM crimes WHERE id = :id")
    fun getCrimeById(id: Int) : Crime

    @Insert
    fun addCrime(crime: Crime) : Long

    @Update
    fun updateCrime(crime: Crime)

    @Update
    fun updateCrimes(crimes: List<Crime>)

    @Delete
    fun deleteCrime(crime: Crime)

    @Delete
    fun deleteAll(crimes: List<Crime>)

    @Query("SELECT COUNT(*) FROM crimes")
    fun getCrimesCount() : Int
}