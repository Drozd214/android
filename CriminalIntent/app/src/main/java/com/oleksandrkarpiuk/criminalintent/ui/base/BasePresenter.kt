package com.oleksandrkarpiuk.criminalintent.ui.base

abstract class BasePresenter : Presenter {

    override fun onCreate() {}

    override fun onStart() {}

    override fun onResume() {}

    override fun onPause() {}

    override fun onStop() {}

    override fun onDestroy() {}

    override fun onCreateView() {}

    override fun onViewCreated() {}

    override fun onDestroyView() {}

}