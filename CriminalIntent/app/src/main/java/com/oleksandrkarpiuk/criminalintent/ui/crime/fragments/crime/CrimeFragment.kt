package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.oleksandrkarpiuk.criminalintent.R
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker.DatePickerFragment
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker.EXTRA_DATE
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker.newInstance
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.timepicker.TimePickerFragment
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.timepicker.newInstance
import com.oleksandrkarpiuk.criminalintent.ui.dataFormat
import com.oleksandrkarpiuk.criminalintent.ui.timeFormat
import kotlinx.android.synthetic.main.fragment_crime.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.text.SimpleDateFormat
import java.util.*

private const val DIALOG_DATE = "DialogDate"
private const val REQUEST_CODE = 0

class CrimeFragment : Fragment(), CrimeContract.View {


    companion object;


    private val presenter: CrimePresenter by inject { parametersOf(this) }




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_crime, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    private fun init() = fetchCrime()?.also {
        presenter.initData(it)
        initCrimeTitleTextView(it.title)
        initCrimeButtons(it.date)
        initCrimeCheckBox(it.isSolved)
        setHasOptionsMenu(true)
    }


    private fun fetchCrime() : Crime? = arguments?.let {
        extractExtras(it).crime
    }


    private fun initCrimeTitleTextView(title: String) = with(crime_title) {
        setText(title)
    }


    private fun initCrimeButtons(date: Date) {
        initDateButton(date)
        initTimeButton(date)
        setCrimeDateTimeText(date)
    }


    private fun initDateButton(date: Date) = with(crime_date) {
        setOnClickListener {
            presenter.onDateClicked()
        }
    }


    private fun initTimeButton(date: Date) = with(crime_time) {
        setOnClickListener {
            presenter.onTimeClicked()
        }
    }


    private fun setCrimeDateTimeText(date: Date) {
        crime_date.text = SimpleDateFormat(dataFormat).format(date)
        crime_time.text = SimpleDateFormat(timeFormat).format(date)
    }


    private fun initCrimeCheckBox(isSolved: Boolean) = with(crime_solved) {
        isChecked = isSolved
        setOnCheckedChangeListener { _, isChecked ->
            presenter.onCheckBoxClicked(isChecked)
        }
    }


    override fun showDatePickerDialog(date: Date) {
        val dialog = DatePickerFragment.newInstance(date)
        dialog.setTargetFragment(this, REQUEST_CODE)
        dialog.show(requireFragmentManager(), DIALOG_DATE)
    }


    override fun showTimePickerDialog(date: Date) {
        val dialog = TimePickerFragment.newInstance(date)
        dialog.setTargetFragment(this, REQUEST_CODE)
        dialog.show(requireFragmentManager(), DIALOG_DATE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode != Activity.RESULT_OK) {
            return
        }

        if(requestCode == REQUEST_CODE) {
            presenter.onActivityResult(data?.getSerializableExtra(EXTRA_DATE) as Date)
        }
    }


    override fun updateDateButtons(date: Date) {
        setCrimeDateTimeText(date)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_item, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.menu_item_delete_crime -> {
                presenter.onDeleteBtnMenuClicked()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun showConfirmation() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.delete_title)
            .setMessage(R.string.delete_message)
            .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.ok) { dialog, which ->
                presenter.onDialogOkButtonClicked()
            }.create()
            .show()

    }


    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }


    override fun finish() {
        activity?.finish()
    }


    override fun getCrimeTitleContent(): String {
        return crime_title.text.toString()
    }


}