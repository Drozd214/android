package com.oleksandrkarpiuk.criminalintent.di

import com.oleksandrkarpiuk.criminalintent.ui.crime.CrimePagerContract
import com.oleksandrkarpiuk.criminalintent.ui.crime.CrimePagerPresenter
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime.CrimeContract
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime.CrimePresenter
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker.DatePickerContract
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker.DatePickerFragment
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker.DatePickerPresenter
import com.oleksandrkarpiuk.criminalintent.ui.crimes.CrimesActivityContract
import com.oleksandrkarpiuk.criminalintent.ui.crimes.CrimesActivityPresenter
import com.oleksandrkarpiuk.criminalintent.ui.crimes.fragment.CrimesContract
import com.oleksandrkarpiuk.criminalintent.ui.crimes.fragment.CrimesPresenter
import org.koin.dsl.module

val mvpPresentersModule = module {

    factory { (view: CrimesActivityContract.View) -> CrimesActivityPresenter(view) }
    factory { (view: CrimesContract.View) -> CrimesPresenter(view, get()) }

    factory { (view: CrimePagerContract.View) ->  CrimePagerPresenter(view, get()) }
    factory { (view: CrimeContract.View) -> CrimePresenter(view, get()) }
    factory { (view: DatePickerContract.View) -> DatePickerPresenter(view) }

}