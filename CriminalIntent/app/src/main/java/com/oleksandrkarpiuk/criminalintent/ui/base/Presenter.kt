package com.oleksandrkarpiuk.criminalintent.ui.base

interface Presenter {

    fun onCreate()

    fun onCreateView()

    fun onViewCreated()

    fun onStart()

    fun onResume()

    fun onPause()

    fun onStop()

    fun onDestroyView()

    fun onDestroy()

}