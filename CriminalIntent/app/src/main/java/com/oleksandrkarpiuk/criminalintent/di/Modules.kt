package com.oleksandrkarpiuk.criminalintent.di

val applicationModules = listOf(
    mvpPresentersModule,
    databaseModule
)