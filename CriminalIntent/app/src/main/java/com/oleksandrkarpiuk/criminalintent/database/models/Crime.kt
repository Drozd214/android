package com.oleksandrkarpiuk.criminalintent.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.oleksandrkarpiuk.criminalintent.database.models.Crime.Companion.TABLE_NAME
import java.io.Serializable
import java.util.*

@Entity(tableName = TABLE_NAME)
data class Crime(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = ID) val id: Int,
    @ColumnInfo(name = TITLE) var title: String,
    @ColumnInfo(name = DATE) var date: Date,
    @ColumnInfo(name = SOLVED) var isSolved: Boolean
) : Serializable {


    companion object {

        const val TABLE_NAME = "crimes"
        const val ID = "id"
        const val TITLE = "title"
        const val DATE = "date"
        const val SOLVED = "isSolved"
    }


    constructor(): this(
        id = 0,
        title = "",
        date = Date(),
        isSolved = false
    )
}