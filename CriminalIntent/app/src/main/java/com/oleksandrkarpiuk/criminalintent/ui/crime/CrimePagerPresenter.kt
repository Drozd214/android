package com.oleksandrkarpiuk.criminalintent.ui.crime

import com.oleksandrkarpiuk.criminalintent.database.daos.CrimeDao
import com.oleksandrkarpiuk.criminalintent.ui.base.BasePresenter

class CrimePagerPresenter(
    private val view: CrimePagerContract.View,
    private val crimeDao: CrimeDao
) : BasePresenter(), CrimePagerContract.ActionListener {


    override fun onCreate() {
        super.onCreate()

        val crimes = crimeDao.getCrimes()
        view.initViewPager(crimes)
    }


}