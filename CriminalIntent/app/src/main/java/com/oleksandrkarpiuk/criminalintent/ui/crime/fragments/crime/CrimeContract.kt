package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime

import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import java.util.*

interface CrimeContract {

    interface View {

        fun showDatePickerDialog(date: Date)

        fun showTimePickerDialog(date: Date)

        fun updateDateButtons(date: Date)

        fun showConfirmation()

        fun finish()

        fun getCrimeTitleContent() : String

    }


    interface ActionListener {

        fun initData(crime: Crime)

        fun onDateClicked()

        fun onTimeClicked()

        fun onActivityResult(date: Date)

        fun onCheckBoxClicked(isSolved: Boolean)

        fun onDeleteBtnMenuClicked()

        fun onDialogOkButtonClicked()
    }

}