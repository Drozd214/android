package com.oleksandrkarpiuk.criminalintent.database

import android.content.Context
import androidx.room.*
import com.oleksandrkarpiuk.criminalintent.database.CriminalIntentDatabase.Companion.VERSION
import com.oleksandrkarpiuk.criminalintent.database.daos.CrimeDao
import com.oleksandrkarpiuk.criminalintent.database.models.Crime

@Database(entities = [Crime::class
], version = VERSION)
@TypeConverters(Converters::class)
abstract class CriminalIntentDatabase : RoomDatabase() {


    companion object {

        const val NAME = "criminalintent_database.db"
        const val VERSION = 3
    }

    abstract val crimeDao: CrimeDao
}