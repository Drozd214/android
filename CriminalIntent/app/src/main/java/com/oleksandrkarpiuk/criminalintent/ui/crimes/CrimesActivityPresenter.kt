package com.oleksandrkarpiuk.criminalintent.ui.crimes

import com.oleksandrkarpiuk.criminalintent.ui.base.BasePresenter

class CrimesActivityPresenter(
    private val view: CrimesActivityContract.View
) : BasePresenter(), CrimesActivityContract.ActionListener {}