package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime

import android.os.Bundle
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import java.io.Serializable

internal object ExtraKeys {

    const val EXTRA_CRIME_ID = "crime_id"

}


internal data class Extras(
    val crime: Crime
)


internal fun extractExtras(args: Bundle) : Extras {
    return Extras(
        crime = args.getSerializable(ExtraKeys.EXTRA_CRIME_ID) as Crime
    )
}


fun CrimeFragment.Companion.newInstance(crime: Crime): CrimeFragment {
    return CrimeFragment()
        .apply {
            arguments = Bundle().apply {
                putSerializable(ExtraKeys.EXTRA_CRIME_ID, crime as Serializable)
            }
        }
}