package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.timepicker

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.oleksandrkarpiuk.criminalintent.R
import kotlinx.android.synthetic.main.dialog_time.view.*
import java.util.*

const val EXTRA_DATE = "date"

class TimePickerFragment : DialogFragment(), TimePickerContract.View {


    companion object;


    private val presenter = TimePickerPresenter(this)
    private lateinit var timeDialog: View




    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        presenter.onCreateDialog(extractExtras(requireArguments()).crimeDate)

        return AlertDialog.Builder(requireContext())
            .setView(timeDialog)
            .setTitle(R.string.time_picker_title)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                presenter.onDialogOkButtonClicked(
                    timeDialog.dialog_time_time_picker.hour,
                    timeDialog.dialog_time_time_picker.minute
                )
            }
            .create()
    }


    override fun setDateToDialog(hours: Int, minutes: Int) {
        timeDialog = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_time, null)
        with(timeDialog.dialog_time_time_picker) {
            hour = hours
            minute = minutes
            setIs24HourView(true)
        }
    }


    override fun sendOkResult(date: Date) {
        targetFragment?.onActivityResult(
            targetRequestCode,
            Activity.RESULT_OK,
            Intent().apply {
                putExtra(EXTRA_DATE, date)
            })
    }
}