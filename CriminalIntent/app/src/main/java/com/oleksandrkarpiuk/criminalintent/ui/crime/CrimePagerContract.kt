package com.oleksandrkarpiuk.criminalintent.ui.crime

import com.oleksandrkarpiuk.criminalintent.database.models.Crime

interface CrimePagerContract {

    interface View {
        fun initViewPager(crimes: List<Crime>)
    }

    interface ActionListener {}

}