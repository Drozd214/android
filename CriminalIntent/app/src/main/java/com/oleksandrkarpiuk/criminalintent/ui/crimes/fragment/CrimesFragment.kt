package com.oleksandrkarpiuk.criminalintent.ui.crimes.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.oleksandrkarpiuk.criminalintent.R
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import com.oleksandrkarpiuk.criminalintent.ui.crime.CrimePagerActivity
import com.oleksandrkarpiuk.criminalintent.ui.crime.newInstance
import kotlinx.android.synthetic.main.fragment_crimes.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.util.*

class CrimesFragment : Fragment(), CrimesContract.View {


    private val presenter: CrimesPresenter by inject { parametersOf(this)}

    private lateinit var crimesAdapter: CrimesAdapter
    private lateinit var menu: Menu




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_crimes, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }


    override fun init(crimes: List<Crime>) {
        setHasOptionsMenu(true)
        initRecycleView(crimes)
    }


    private fun initRecycleView(crimes: List<Crime>) = with(crime_recycle_view) {
        layoutManager = initLayoutManager()
        adapter = initAdapter(crimes)

        addItemDecoration(DividerItemDecoration(crime_recycle_view.context, LinearLayoutManager.VERTICAL))
    }


    private fun initLayoutManager() : LinearLayoutManager {
        return LinearLayoutManager(requireContext())
    }


    private fun initAdapter(crimes: List<Crime>) : CrimesAdapter {
        return CrimesAdapter(crimes).apply {
            onItemClickListener = { crime ->
                presenter.onItemClicked(crime)
            }

            onCheckedChangeListener = {crime, position, isChecked ->
                presenter.onItemCheckBoxClicked(crime, position, isChecked)
            }
        }.also {
            crimesAdapter = it
        }
    }


    override fun launchCrimePagerActivity(crime: Crime) {
        startActivity(
            CrimePagerActivity.newInstance(
                requireContext(),
                crime
            ))
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_crime_list, menu)
        this.menu = menu
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.menu_item_new_crime -> {
                presenter.onAddBtnMenuClicked()
                true
            }

            R.id.menu_item_show_subtitle -> {
                presenter.onShowSubtitleBtnMenuClicked()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun showSubtitle(subtitle: String?) {
        (activity as AppCompatActivity).supportActionBar?.subtitle = subtitle
    }


    override fun setSubtitleMenuText(subtitleTextId: Int) {
        menu.findItem(R.id.menu_item_show_subtitle).title = getString(subtitleTextId)
    }


    override fun showHint() {
        hintTv.visibility = View.VISIBLE
    }


    override fun hideHint() {
        hintTv.visibility = View.GONE
    }


    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }


    override fun refreshScreen(crimes: List<Crime>) {
        crimesAdapter.crimes = crimes.toMutableList()
        crimesAdapter.notifyDataSetChanged()
    }


    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }


}