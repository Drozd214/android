package com.oleksandrkarpiuk.criminalintent.ui.crimes.fragment

import com.oleksandrkarpiuk.criminalintent.R
import com.oleksandrkarpiuk.criminalintent.database.daos.CrimeDao
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import com.oleksandrkarpiuk.criminalintent.ui.base.BasePresenter
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.coroutineScope
import org.koin.core.scope.Scope
import java.text.FieldPosition

class CrimesPresenter(
    private val view: CrimesContract.View,
    private val crimeDao: CrimeDao
) : BasePresenter(), CrimesContract.ActionListener {


    private var isSubtitleVisible = false
    private lateinit var crimes: MutableList<Crime>


    override fun onViewCreated() {
        super.onViewCreated()
        //crimeDao.deleteAll(crimeDao.getCrimes())
        crimes = crimeDao.getCrimes().toMutableList()
        view.init(crimes)
    }


    override fun onItemClicked(crime: Crime) {
        view.launchCrimePagerActivity(crime)
    }


    override fun onItemCheckBoxClicked(crime: Crime, position: Int, isSolved: Boolean) {
        val newCrime = crime.copy(isSolved = isSolved)
        crimes[position] = newCrime
    }


    override fun onAddBtnMenuClicked() {
        val newCrime = Crime()
        val id = crimeDao.addCrime(newCrime)


        val crime = crimeDao.getCrimeById(id.toInt())
        view.launchCrimePagerActivity(crime)
    }


    override fun onShowSubtitleBtnMenuClicked() {
        if(isSubtitleVisible) {
            view.showSubtitle(null)
            view.setSubtitleMenuText(R.string.show_subtitle)

        } else {
            val crimesCount = crimes.size
            view.showSubtitle("$crimesCount crimes")
            view.setSubtitleMenuText(R.string.hide_subtitle)
        }

        isSubtitleVisible = !isSubtitleVisible
    }


    override fun onResume() {
        super.onResume()
        crimes = crimeDao.getCrimes().toMutableList()

        if(crimes.size == 0) {
            view.showHint()
        } else {
            view.refreshScreen(crimes)
            view.hideHint()
        }

        if(isSubtitleVisible) {
            isSubtitleVisible = false
            onShowSubtitleBtnMenuClicked()
        }
    }


    override fun onPause() {
        super.onPause()
        crimeDao.updateCrimes(crimes)
    }


}