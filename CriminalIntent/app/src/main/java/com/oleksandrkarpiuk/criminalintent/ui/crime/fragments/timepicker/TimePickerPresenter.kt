package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.timepicker

import com.oleksandrkarpiuk.criminalintent.ui.base.BasePresenter
import java.util.*

class TimePickerPresenter(
    private val view: TimePickerContract.View
) : BasePresenter(), TimePickerContract.ActionListener {


    private lateinit var date: Date

    override fun onCreateDialog(date: Date) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.time = date
        this.date = date

        view.setDateToDialog(
            calendar.get(Calendar.HOUR),
            calendar.get(Calendar.MINUTE)
        )
    }


    override fun onDialogOkButtonClicked(hours: Int, minutes: Int) {
        date.hours = hours
        date.minutes = minutes

        view.sendOkResult(date)
    }


}