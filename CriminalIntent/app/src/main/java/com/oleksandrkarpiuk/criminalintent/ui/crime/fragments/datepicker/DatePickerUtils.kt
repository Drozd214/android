package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker

import android.os.Bundle
import java.util.*

internal object ExtraKeys {

    const val ARG_DATE = "date"

}


internal data class Extras(
    val crimeDate: Date
)


internal fun extractExtras(args: Bundle) : Extras {
    return Extras(
        crimeDate = args.getSerializable(ExtraKeys.ARG_DATE) as Date
    )
}


fun DatePickerFragment.Companion.newInstance(date: Date) : DatePickerFragment {
    return DatePickerFragment()
        .apply {
            arguments = Bundle().apply {
                putSerializable(ExtraKeys.ARG_DATE, date)
            }
        }
}