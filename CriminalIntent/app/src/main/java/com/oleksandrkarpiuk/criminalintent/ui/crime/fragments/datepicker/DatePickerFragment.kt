package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.oleksandrkarpiuk.criminalintent.R
import kotlinx.android.synthetic.main.dialog_date.view.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.util.*

const val EXTRA_DATE = "date"

class DatePickerFragment : DialogFragment(), DatePickerContract.View {


    companion object;


    private val presenter: DatePickerPresenter by inject { parametersOf(this)}
    private lateinit var dateDialog: View




    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        presenter.onCreateDialog(extractExtras(requireArguments()).crimeDate)

        return AlertDialog.Builder(requireContext())
            .setView(dateDialog)
            .setTitle(R.string.date_picker_title)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                presenter.onDialogOkButtonClicked(
                    dateDialog.dialog_date_date_picker.year,
                    dateDialog.dialog_date_date_picker.month,
                    dateDialog.dialog_date_date_picker.dayOfMonth
                )
            }
            .create()
    }


    override fun setDateToDialog(year: Int, month: Int, day: Int) {
        dateDialog = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_date, null)
        dateDialog.dialog_date_date_picker.init(year, month, day, null)
    }


    override fun sendOkResult(date: Date) {
        targetFragment?.onActivityResult(
            targetRequestCode,
            Activity.RESULT_OK,
            Intent().apply {
                putExtra(EXTRA_DATE, date)
            })
    }


}