package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker

import com.oleksandrkarpiuk.criminalintent.ui.base.BasePresenter
import java.util.*

class DatePickerPresenter(
    private val view: DatePickerContract.View
) : BasePresenter(), DatePickerContract.ActionListener {


    override fun onCreateDialog(date: Date) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.time = date

        view.setDateToDialog(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))
    }


    override fun onDialogOkButtonClicked(year: Int, month: Int, dayOfMonth: Int) {
        view.sendOkResult(
            GregorianCalendar(year, month, dayOfMonth).time
        )
    }


}