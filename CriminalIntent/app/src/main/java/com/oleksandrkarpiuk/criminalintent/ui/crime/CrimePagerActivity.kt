package com.oleksandrkarpiuk.criminalintent.ui.crime

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.oleksandrkarpiuk.criminalintent.R
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import com.oleksandrkarpiuk.criminalintent.ui.base.SingleFragmentActivity
import kotlinx.android.synthetic.main.activity_crime_pager.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class CrimePagerActivity : AppCompatActivity(), CrimePagerContract.View {


    companion object;


    private val presenter: CrimePagerPresenter by inject { parametersOf(this) }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crime_pager)

        presenter.onCreate()
    }


    override fun initViewPager(crimes: List<Crime>) = with(crime_view_pager) {
        val crime = extractExtras(intent).crime

        adapter = CrimePagerAdapter(supportFragmentManager, crimes)
        currentItem = crimes.indexOf(crime)
    }


}