package com.oleksandrkarpiuk.criminalintent.ui.crimes.fragment

import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import java.util.*

interface CrimesContract {

    interface View {

        fun init(crimes: List<Crime>)

        fun refreshScreen(crimes: List<Crime>)

        fun launchCrimePagerActivity(crime: Crime)

        fun showSubtitle(subtitle: String?)

        fun setSubtitleMenuText(subtitleTextId: Int)

        fun showHint()

        fun hideHint()

    }


    interface ActionListener {

        fun onItemClicked(crime: Crime)

        fun onItemCheckBoxClicked(crime: Crime, position: Int, isSolved: Boolean)

        fun onAddBtnMenuClicked()

        fun onShowSubtitleBtnMenuClicked()
    }

}