package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.timepicker

import java.util.*

interface TimePickerContract {

    interface View {

        fun setDateToDialog(hours: Int, minutes: Int)

        fun sendOkResult(date: Date)
    }


    interface ActionListener {

        fun onCreateDialog(date: Date)

        fun onDialogOkButtonClicked(hours: Int, minutes: Int)
    }

}