package com.oleksandrkarpiuk.criminalintent.ui.crimes

interface CrimesActivityContract {

    interface View {}

    interface ActionListener {}

}