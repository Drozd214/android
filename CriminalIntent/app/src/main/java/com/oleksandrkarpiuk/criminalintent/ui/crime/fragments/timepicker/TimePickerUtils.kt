package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.timepicker

import android.os.Bundle
import java.util.*

internal object ExtraKeys {

    const val ARG_DATE = "date"

}


internal data class Extras(
    val crimeDate: Date
)


internal fun extractExtras(args: Bundle) : Extras {
    return Extras(
        crimeDate = args.getSerializable(ExtraKeys.ARG_DATE) as Date
    )
}


fun TimePickerFragment.Companion.newInstance(date: Date) : TimePickerFragment {
    return TimePickerFragment()
        .apply {
            arguments = Bundle().apply {
                putSerializable(ExtraKeys.ARG_DATE, date)
            }
        }
}