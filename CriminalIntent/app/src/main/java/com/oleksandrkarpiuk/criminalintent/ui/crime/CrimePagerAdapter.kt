package com.oleksandrkarpiuk.criminalintent.ui.crime

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime.CrimeFragment
import com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime.newInstance

class CrimePagerAdapter(
    fragmentManager: FragmentManager,
    private val crimes: List<Crime>
//TODO знайти новіший альтернативний клас
) : FragmentPagerAdapter(fragmentManager) {


    override fun getItem(position: Int): Fragment {
        val crime = crimes[position]
        return CrimeFragment.newInstance(crime)
    }


    override fun getCount(): Int {
        return crimes.size
    }


}