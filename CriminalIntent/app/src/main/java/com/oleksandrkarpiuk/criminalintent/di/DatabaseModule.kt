package com.oleksandrkarpiuk.criminalintent.di

import androidx.room.Room
import com.oleksandrkarpiuk.criminalintent.database.CriminalIntentDatabase
import org.koin.dsl.module

val databaseModule = module {

    single {
        Room.databaseBuilder(
            get(),
            CriminalIntentDatabase::class.java,
            CriminalIntentDatabase.NAME
        ).allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    single { get<CriminalIntentDatabase>().crimeDao }
}