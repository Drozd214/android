package com.oleksandrkarpiuk.criminalintent

import android.app.Application
import com.oleksandrkarpiuk.criminalintent.di.applicationModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CriminalIntentApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CriminalIntentApplication)
            modules(applicationModules)
        }
    }
}