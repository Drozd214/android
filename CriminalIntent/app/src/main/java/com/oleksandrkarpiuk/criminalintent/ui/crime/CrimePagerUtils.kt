package com.oleksandrkarpiuk.criminalintent.ui.crime

import android.content.Context
import android.content.Intent
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import java.util.*

internal object ExtraKeys {

    const val EXTRA_CRIME_ID = "crime_id"

}


internal data class Extras(
    val crime: Crime
)


internal fun extractExtras(intent: Intent) : Extras {
    return Extras(
        crime = intent.getSerializableExtra(ExtraKeys.EXTRA_CRIME_ID) as Crime
    )
}


fun CrimePagerActivity.Companion.newInstance(context: Context, crime: Crime): Intent {
    return Intent(context, CrimePagerActivity::class.java).apply {
        putExtra(ExtraKeys.EXTRA_CRIME_ID, crime)
    }
}