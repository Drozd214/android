package com.oleksandrkarpiuk.criminalintent.ui.crimes

import androidx.fragment.app.Fragment
import com.oleksandrkarpiuk.criminalintent.ui.base.SingleFragmentActivity
import com.oleksandrkarpiuk.criminalintent.ui.crimes.fragment.CrimesFragment
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class CrimesActivity: SingleFragmentActivity(), CrimesActivityContract.View {


    private val presenter: CrimesActivityPresenter by inject { parametersOf(this) }




    override fun createFragment(): Fragment {
        return CrimesFragment()
    }


}