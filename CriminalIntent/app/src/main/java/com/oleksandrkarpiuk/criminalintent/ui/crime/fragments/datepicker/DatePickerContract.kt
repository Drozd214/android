package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.datepicker

import java.time.Month
import java.util.*

interface DatePickerContract {

    interface View {

        fun setDateToDialog(year: Int, month: Int, day: Int)

        fun sendOkResult(date: Date)
    }


    interface ActionListener {

        fun onCreateDialog(date: Date)

        fun onDialogOkButtonClicked(year: Int, month: Int, dayOfMonth: Int)
    }

}