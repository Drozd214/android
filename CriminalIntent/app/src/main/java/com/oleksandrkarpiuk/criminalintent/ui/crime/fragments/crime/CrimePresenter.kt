package com.oleksandrkarpiuk.criminalintent.ui.crime.fragments.crime

import com.oleksandrkarpiuk.criminalintent.database.daos.CrimeDao
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import com.oleksandrkarpiuk.criminalintent.ui.base.BasePresenter
import java.util.*

class CrimePresenter(
    private val view: CrimeContract.View,
    private val crimeDao: CrimeDao
) : BasePresenter(), CrimeContract.ActionListener {


    private lateinit var crime: Crime




    override fun initData(crime: Crime) {
        this.crime = crime
    }


    override fun onDateClicked() {
        view.showDatePickerDialog(crime.date)
    }


    override fun onTimeClicked() {
        view.showTimePickerDialog(crime.date)
    }


    override fun onCheckBoxClicked(isSolved: Boolean) {
        crime.isSolved = isSolved
        crimeDao.updateCrime(crime)
    }


    override fun onActivityResult(date: Date) {
        crime = crime.copy(date = date)
        crimeDao.updateCrime(crime)
        view.updateDateButtons(crime.date)
    }


    override fun onDeleteBtnMenuClicked() {
        view.showConfirmation()
    }


    override fun onDialogOkButtonClicked() {
        crimeDao.deleteCrime(crime)
        view.finish()
    }


    override fun onPause() {
        super.onPause()
        var title = view.getCrimeTitleContent()

        if(title == "") {
            title = "Crime ${crime.id}"
        }

        crime = crime.copy(title = title)
        crimeDao.updateCrime(crime)
    }


}