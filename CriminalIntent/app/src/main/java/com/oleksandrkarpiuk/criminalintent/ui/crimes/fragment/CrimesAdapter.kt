package com.oleksandrkarpiuk.criminalintent.ui.crimes.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oleksandrkarpiuk.criminalintent.R
import com.oleksandrkarpiuk.criminalintent.database.CriminalIntentDatabase
import com.oleksandrkarpiuk.criminalintent.database.models.Crime
import com.oleksandrkarpiuk.criminalintent.ui.dataFormat
import com.oleksandrkarpiuk.criminalintent.ui.timeFormat
import kotlinx.android.synthetic.main.crime_item.view.*
import java.text.SimpleDateFormat

class CrimesAdapter(
    crimes: List<Crime>
) : RecyclerView.Adapter<CrimeHolder>() {


    var crimes = crimes
    var onItemClickListener: ((Crime) -> Unit)? = null
    var onCheckedChangeListener: ((Crime, Int, Boolean) -> Unit)? = null




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CrimeHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.crime_item, parent, false)
        return CrimeHolder(itemView)
    }


    override fun getItemCount(): Int {
        return crimes.size
    }


    override fun onBindViewHolder(holder: CrimeHolder, position: Int) {
        val crime = crimes[position]
        with(holder) {
            bindCrime(crime, position, onCheckedChangeListener)
            setOnItemClickListener(
                crime = crime,
                listener = onItemClickListener
            )
        }
    }


}


class CrimeHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


    private val title: TextView = itemView.titleCrimeItemTv
    private val date: TextView = itemView.dateCrimeItemTv
    private val solved: CheckBox = itemView.solvedCrimeItemCb




    fun bindCrime(crime: Crime,
                  position: Int,
                  listener: ((Crime, Int, Boolean) -> Unit)?) {
        title.text = crime.title
        date.text = "${SimpleDateFormat(dataFormat).format(crime.date)}   ${SimpleDateFormat(timeFormat).format(crime.date)}"

        solved.setOnCheckedChangeListener(null)
        solved.isChecked = crime.isSolved
        solved.setOnCheckedChangeListener { _, isChecked ->
            listener?.invoke(crime, position, isChecked)
        }
    }


    fun setOnItemClickListener(crime: Crime,
                               listener: ((Crime) -> Unit)?) {
        itemView.setOnClickListener {
            listener?.invoke(crime)
        }
    }


}